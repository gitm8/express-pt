const debug = require('debug')('xpress:connection');

let connections = [];


function noOpenConnections() {
  return connections.length === 0;
}

/**
 * To be able to properly handle shutdown scenarios, we store
 * a list of connections in memory. Once a connection is closed,
 * we remove it from the list.
 */
function addConnectionTracker(instance) {
  let connectionId = 0;

  // handle connections
  instance.on('connection', (connection) => {
    connectionId += 1;
    const conId = String(connectionId).padStart(7, '0');
    const then = Date.now();

    debug(`[+] connection opened [ ${conId} ]`);

    connection._id = conId; // eslint-disable-line

    // store new connection in connections collector
    connections = [...connections, connection];

    connection.once('close', () => {
      debug(`[-] connection closed [ ${conId} ] ${Date.now() - then}ms`);
      connections = connections.filter(c => c !== connection);
    });
  });

  return instance;
}

function closeConnection(con, forceShutdownAfter) {
  return new Promise((resolve) => {
    debug(`asking connection to close ${con._id}`); // eslint-disable-line
    con.end();

    // if connection doesn't want to close, force it
    const killConTimeout = setTimeout(() => con.destroy(), forceShutdownAfter);

    const handleClose = () => {
      clearTimeout(killConTimeout);
      resolve(con);
    };

    con.once('close', handleClose);
  });
}

function closeConnections(graceTimeout, killTimeout) {
  const startTime = Date.now();

  return new Promise((resolve) => {
    const checkConnectionInterval = setInterval(() => {
      const inGracePeriod = startTime + graceTimeout > Date.now();


      if (inGracePeriod && noOpenConnections()) {
        debug('No connections open. Closing the server.');
        clearInterval(checkConnectionInterval);
        resolve();
      } if (!inGracePeriod && !noOpenConnections()) {
        debug('Grace period over. Closing remaining connections.');
        clearInterval(checkConnectionInterval);

        const toCloseConnectionHandler = con => closeConnection(con, killTimeout);
        const closingConnections = connections.map(toCloseConnectionHandler);

        resolve(Promise.all(closingConnections).then(() => debug('All connections closed.')));
      }
    }, 100);
  });
}

module.exports = {
  closeConnection,
  closeConnections,
  noOpenConnections,
  addConnectionTracker,
};
