const debug = require('debug')('xpress:close');

function signalHandler(signal, express) {
  // prevent process from halting
  // https://nodejs.org/api/process.html#process_signal_events
  return () => {
    process.stdin.resume();
    debug(`Received ${signal}`);
    express.close().then(() => process.exit());
  };
}

function addProcessHandler(express) {
  // throw consistently on unhandled rejections
  process.on('unhandledRejection', (err) => { throw err; });

  /**
   * Signal handler
   */
  process.once('SIGINT', signalHandler('SIGINT', express));
  process.once('SIGTERM', signalHandler('SIGTERM', express));
  process.once('SIGHUP', signalHandler('SIGHUP', express));

  // https://github.com/remy/nodemon#controlling-shutdown-of-your-script
  process.once('SIGUSR2', signalHandler('SIGUSR2', express));
}
module.exports = {
  signalHandler,
  addProcessHandler,
};
