const debug = require('debug')('xpress:close');
const { closeConnections, noOpenConnections } = require('../connections');


function createExpressClose(express) {
  return function xpressServerClose(callback = () => {}) {
    debug('Starting to close down server.');
    // 1/3 of the timeout is spent waiting for connections to close naturally
    const graceTimeout = express.get('shutdownTimeout') * 0.66;
    // the rest of the time is spent ending or ultimatly destroying pending connections
    const killTimeout = express.get('shutdownTimeout') * 0.33;
    const server = express.get('instance');

    // stop accepting new connections
    server.close();

    if (noOpenConnections()) {
      return express.afterClose.run(express)
        .then(() => callback())
        .then(() => {
          debug('Server is closed.');
        })
        .catch(callback);
    }

    debug('stopped accepting new connections');

    return closeConnections(graceTimeout, killTimeout)
      .then(() => express.afterClose.run(express))
      .then(() => callback())
      .then(() => {
        debug('Server is closed.');
      })
      .catch(callback);
  };
}

module.exports = {
  createExpressClose,
};
