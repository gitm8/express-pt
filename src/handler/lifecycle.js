const debugr = require('debug');

function chain(promises) {
  if (promises.length === 0) {
    return Promise.resolve();
  }

  const nextPromise = promises[0];
  const remainingPromises = promises.slice(1);

  return nextPromise()
    .then(() => chain(remainingPromises));
}

function toFunctionName(fn) {
  return `[Function${fn.name ? `: ${fn.name}]` : ']'}`;
}

function createHandler(handler, lifecycle) {
  const debug = debugr(`xpress:${lifecycle}`);

  return express => new Promise((resolve, reject) => {
    try {
      return Promise.resolve(handler(express))
        .then(() => {
          debug(`run ${lifecycle} handler: ${toFunctionName(handler)}`);
          resolve();
        });
    } catch (err) {
      debug(err);
      debug(`failed to run ${lifecycle} handler: ${toFunctionName(handler)}`);
      return reject(err);
    }
  });
}

function createLifecycleCollector(lifecycle) {
  const debug = debugr(`xpress:${lifecycle}`);
  let handlerCollection = [];

  function addLifecycleHandler(handler) {
    debugr('xpress:lifecycles')(`add ${lifecycle} handler: ${toFunctionName(handler)}`);

    handlerCollection = [ // eslint-disable-line
      ...handlerCollection,
      createHandler(handler, lifecycle),
    ];
  }

  addLifecycleHandler.run = function runLifecycleHandler(express) {
    if (handlerCollection.length === 0) {
      debug(`no ${lifecycle} handler found.`);
      return Promise.resolve([]);
    }

    debug(`running ${lifecycle} handler.`);
    return chain(handlerCollection.map(h => () => h(express)));
  };

  return addLifecycleHandler;
}

module.exports = {
  createHandler,
  toFunctionName,
  createLifecycleCollector,
};
