const debug = require('debug')('xpress:listen');

function createExpressListen(express) {
  return function xpressServerListen(...args) {
    const server = express.get('instance');
    const maxAttempts = express.get('attemptsToListen');
    const calledWithCallback = typeof args[args.length - 1] === 'function';
    const callback = calledWithCallback ? args[args.length - 1] : () => {};

    const listen = function serverListen(...listenArgs) {
      return new Promise((resolve, reject) => {
        let attemptToStart = 1;
        server
          .listen(...listenArgs)
          .on('error', (err) => {
            if (err.code === 'EADDRINUSE' && attemptToStart < maxAttempts) {
              debug(`Unable to bind to ${listenArgs[0]}. Retrying in ${1 * attemptToStart} seconds.`);
              setTimeout(() => {
                server.emit('retryListen', attemptToStart);
                server
                  .close()
                  .listen(...listenArgs);
              }, 1000 * attemptToStart++); // eslint-disable-line no-plusplus
            } else {
              reject(err);
            }
          })
          .once('listening', resolve);
      });
    };

    express.set('instance', server);

    /**
     * This promise will be handled regardless if listen was called
     * with a callback or not. The actual listen will be called
     * AFTER the beforeListener handlers are run.
     */
    const listenPromise = express.beforeListen.run(express)
      // actually start listening
      .then(() => listen(...args))
      .then(() => {
        const { address, port } = server.address();
        const startUpTime = Date.now() - express.get('initializedAt');
        const nodeEnv = express.get('env');

        debug(`Server listening on ${address}:${port} after ${startUpTime}ms (mode: ${nodeEnv})`);
      })
      .catch((err) => {
        debug(`Encountered error during initialization: ${err.message}`);
        callback(err);
        return express.close()
          .then(() => {
            debug(err);
            debug('Failed to start server.');
            debug(express.get('manageProcess'));
            if (express.get('manageProcess')) {
              process.exit(1);
            }
            if (calledWithCallback) {
              callback(err);
            } else {
              throw err;
            }
          });
      });


    return calledWithCallback ? server : listenPromise;
  };
}

module.exports = {
  createExpressListen,
};
