const { createServer } = require('http');
const express = require('express');
const debug = require('debug')('xpress:main');

const { createLifecycleCollector } = require('./handler/lifecycle');
const { createExpressListen } = require('./handler/listen');
const { createExpressClose } = require('./handler/close');
const { addProcessHandler } = require('./process');
const { addConnectionTracker } = require('./connections');

const defaultOptions = {
  manageProcess: false,
  shutdownTimeout: 3000,
  attemptsToListen: 3,
};

function createApplication(options = {}) {
  debug('Bootstrapping xpress server.');

  const app = express();
  const baseServer = createServer(app);
  const server = addConnectionTracker(baseServer);

  const config = {
    ...defaultOptions,
    ...options,
  };

  // https://expressjs.com/en/advanced/best-practice-security.html#use-helmet
  app.disable('x-powered-by');

  app.set('instance', server);
  app.set('manageProcess', config.manageProcess);
  app.set('shutdownTimeout', config.shutdownTimeout);
  app.set('attemptsToListen', config.attemptsToListen);
  app.set('initializedAt', Date.now());

  // app.get('/metrics', (req, res) => {
  //   res.json({
  //     uptime: process.uptime(),
  //     arch: process.arch,
  //     platform: process.platform,
  //     nodeVersion: process.version,
  //     release: process.release,
  //     argv: process.argv,
  //     cpu: process.cpuUsage(),
  //     cwd: process.cwd(),
  //     env: process.env,
  //     nodeArgs: process.execArgv,
  //     execPath: process.execPath,
  //     gid: process.getegid(),
  //     uid: process.geteuid(),
  //     pid: process.pid,
  //     ppid: process.ppid,
  //     memory: process.memoryUsage(),
  //   });
  // });

  app.beforeListen = createLifecycleCollector('beforeListen');
  app.afterClose = createLifecycleCollector('afterClose');
  app.close = createExpressClose(app);
  app.listen = createExpressListen(app);

  if (config.manageProcess) addProcessHandler(app);


  return app;
}

Object.keys(express).forEach((prop) => {
  // eslint-disable-next-line
  createApplication[prop] = express[prop];
});

module.exports = createApplication;
