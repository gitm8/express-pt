const express = require('../../../src');

const HANDLE = process.argv[2];
const app = express({ manageProcess: true });

app.listen(HANDLE)
  .then(() => app.close());
