const app = require('../../../src')({ manageProcess: true });

app.afterClose(() => {
  console.log(Object.keys(process));
  process.send({ type: 'afterClose' });
});

app.listen()
  .then(() => {
    process.send({ type: 'listening' });
  });
