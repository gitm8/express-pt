/* eslint no-unused-expressions: 0 */
const os = require('os');
const path = require('path');
const http = require('http');
const { spawn, spawnSync } = require('child_process');

const { expect } = require('chai');
const sinon = require('sinon');

const xpress = require('../../src');

const randomString = () => Math.random().toString(36).substring(2, 15);

function request(socketPath, urlPath, options = {}) {
  return new Promise((resolve, reject) => {
    const req = http.request(
      Object.assign(
        {},
        {
          socketPath,
          path: urlPath,
          method: 'GET',
        },
        options,
      ),
      (response) => {
        let data = '';
        response
          .setEncoding('utf8')
          .on('data', (chunk) => { data += chunk; })
          .on('end', () => { resolve(data); });
      },
    );

    if (options.throw) {
      req.on('error', reject);
    } else {
      req.on('error', resolve);
    }
    req.end();
  });
}

const getTmpSocketAdress = () => path.join(os.tmpdir(), `xpress-test-socket-${randomString()}`);
const lifecycleErrorMessage = '¯\\_(ツ)_/¯';
const lifecycleError = new Error(lifecycleErrorMessage);
const faultyLifecycleHandler = () => { throw lifecycleError; };
const pingHandler = (req, res) => res.send('pong');
const sendPing = socketPath => request(socketPath, '/ping');
const closeAfter = (srvr, t) => new Promise(
  resolve => setTimeout(() => srvr.close(resolve), t),
);
const startAfter = (srv, handle, t) => new Promise(
  resolve => setTimeout(() => srv.listen(handle).then(resolve), t),
);

describe('functional tests', () => {
  it('can start and stop a server on a given port/path/handle', async () => {
    const server = xpress();
    const socket = getTmpSocketAdress();

    server.get('/ping', pingHandler);

    await server.listen(socket);
    expect(server.get('instance').listening).to.be.true;
    expect(await sendPing(socket)).to.equal('pong');

    await server.close();
    expect(server.get('instance').listening).to.be.false;
  });

  describe('listen', () => {
    it('can be called with a callback', () => new Promise((resolve) => {
      const server = xpress();

      const instance = server.listen(getTmpSocketAdress(), () => {
        expect(instance.listening).to.be.true;
        setTimeout(
          () => { // wait a tick for the serevr to be fully initiated
            server.close()
              .then(() => {
                expect(instance.listening).to.be.false;
                resolve();
              });
          },
          0,
        );
      });
    }));

    it('calls callback with an error on exception during startup', (done) => {
      const server = xpress();
      const callbackSpy = sinon.spy();

      server.beforeListen(faultyLifecycleHandler);

      try {
        server.listen(getTmpSocketAdress(), callbackSpy);
        expect(callbackSpy.calledWith(lifecycleError)).to.be.true;
        done();
      } catch (err) {
        done();
      }
    });

    it('throws if an error happens during startup (no manageProcess)', async () => {
      const server = xpress();

      server.beforeListen(faultyLifecycleHandler);

      try {
        await server.listen(getTmpSocketAdress());
      } catch (err) {
        expect(err.message).to.equal(lifecycleErrorMessage);
      }
    });

    it('returns a promise when called without callback', async () => {
      const server = xpress();

      const listenPromise = server.listen(getTmpSocketAdress());

      expect(listenPromise instanceof Promise).to.be.true;

      await listenPromise;
      await server.close();
    });

    it('returns server instance when called with callback (legacy usage)', () => {
      const server = xpress();

      const instance = server.listen(getTmpSocketAdress(), async () => {
        expect(instance instanceof http.Server).to.be.true;

        await server.close();
      });
    });

    it('retries startup on used listen handle.', async () => {
      const address = getTmpSocketAdress();
      const server = xpress();
      const server2 = xpress();
      const instance = server2.get('instance');
      const retrySpy = sinon.spy();
      const listenSpy = sinon.spy();

      instance.on('retryListen', retrySpy);
      instance.once('listening', listenSpy);

      await Promise.all([
        server.listen(address),
        startAfter(server2, address, 50),
        closeAfter(server, 100),
        closeAfter(server2, 1200),
      ]);

      expect(retrySpy.callCount).to.equal(1);
      expect(listenSpy.calledOnce).to.be.true;
    });

    it('stops to connect to used handle after 3 tries.', async function preserveThis() {
      this.timeout(5000);

      const address = getTmpSocketAdress();
      const server = xpress();
      const server2 = xpress();
      const instance = server2.get('instance');
      const retrySpy = sinon.spy();
      const listenSpy = sinon.spy();

      instance.on('retryListen', retrySpy);
      instance.once('listening', listenSpy);

      await server.listen(address)
        .then(() => server2.listen(address))
        .catch(async (err) => {
          await server.close();

          expect(err.code).to.equal('EADDRINUSE');
          expect(retrySpy.callCount).to.equal(2);
          expect(listenSpy.calledOnce).to.be.false;
        });
    });
  });

  describe('close', () => {
    it('grants a grace period for connections to finish', async () => {
      const server = xpress({ shutdownTimeout: 90 });
      const adress = getTmpSocketAdress();

      server.get('/first', (req, res) => res.send('pong'));
      server.get('/second', (req, res) => setTimeout(() => { res.send('pong'); }, 30));
      server.get('/third', (req, res) => setTimeout(() => { res.send('will not reach'); }, 120));

      await server.listen(adress);

      const res = await Promise.all([
        request(adress, '/first'),
        request(adress, '/second'),
        request(adress, '/third', { throw: false }),
        closeAfter(server, 10),
      ]);

      expect(res[0]).to.equal('pong');
      expect(res[1]).to.equal('pong');
      expect(res[2].code).to.equal('ECONNRESET');
    });

    // if this test fails, first try to adjust the timeouts in the test
    // the timouts are set pretty small to run tests fast. This could go
    // wrong depending on the underlying machine.
    it('terminates connections open longer than grace period', async () => {
      const server = xpress({ shutdownTimeout: 90 });
      const adress = getTmpSocketAdress();

      server.get('/first', (req, res) => res.send('pong'));
      server.get('/second', (req, res) => setTimeout(() => { res.send('pong'); }, 120));

      await server.listen(adress);

      const res = await Promise.all([
        request(adress, '/first'),
        request(adress, '/second', { throw: false }),
        closeAfter(server, 10),
      ]);

      expect(res[0]).to.equal('pong');
      expect(res[1].code).to.equal('ECONNRESET');
    });

    it('accepts a callback for close.', async () => {
      const server = xpress();
      const adress = getTmpSocketAdress();
      const callback = sinon.spy();

      await server.listen(adress);
      expect(callback.notCalled).to.be.true;
      await server.close(callback);
      expect(callback.calledOnce).to.be.true;
    });

    it('calls callback with an error on exception during close .', async () => {
      const server = xpress();
      const adress = getTmpSocketAdress();
      const callback = sinon.spy();

      server.afterClose(faultyLifecycleHandler);

      await server.listen(adress);
      expect(callback.notCalled).to.be.true;
      await server.close(callback);
      expect(callback.calledWith(lifecycleError)).to.be.true;
    });
  });

  describe('beforeListen', () => {
    it('sets handler to be called before `server.listen`', async () => {
      const server = xpress();
      const onStartHandler = sinon.spy();

      server.beforeListen(onStartHandler);

      expect(onStartHandler.called).to.be.false;

      await server.listen(getTmpSocketAdress());

      expect(onStartHandler.calledOnce).to.be.true;

      await server.close();
    });

    it('sets multiple handler to be called before `server.listen`', async () => {
      const server = xpress();
      const firstOnStartHandler = sinon.spy();
      const secondOnStartHandler = sinon.spy();

      server.beforeListen(firstOnStartHandler);
      server.beforeListen(secondOnStartHandler);

      expect(firstOnStartHandler.called).to.be.false;
      expect(secondOnStartHandler.called).to.be.false;

      await server.listen(getTmpSocketAdress());

      expect(firstOnStartHandler.calledOnce).to.be.true;
      expect(secondOnStartHandler.calledOnce).to.be.true;

      await server.close();
    });

    it('executes multiple handler consecutively before `server.listen`', async () => {
      const server = xpress();
      const firstOnStartHandler = sinon.spy();
      const secondOnStartHandler = sinon.spy();

      server.beforeListen(firstOnStartHandler);
      server.beforeListen(secondOnStartHandler);

      await server.listen(getTmpSocketAdress());
      await server.close();

      expect(firstOnStartHandler.calledBefore(secondOnStartHandler)).to.be.true;
      expect(secondOnStartHandler.calledAfter(firstOnStartHandler)).to.be.true;
    });

    it('will stop further execution if a handler throws.', async () => {
      const server = xpress();
      const firstOnStartHandler = sinon.spy();
      const thirdStartHandler = sinon.spy();

      server.beforeListen(firstOnStartHandler);
      server.beforeListen(faultyLifecycleHandler);
      server.beforeListen(thirdStartHandler);

      try {
        await server.listen(getTmpSocketAdress());
      } catch (e) {
        expect(firstOnStartHandler.calledOnce).to.be.true;
        expect(thirdStartHandler.notCalled).to.be.true;
      }
    });
  });

  describe('afterClose', () => {
    it('sets handler to be called after `server.close`', async () => {
      const server = xpress();
      const afterCloseHandler = sinon.spy();
      server.afterClose(afterCloseHandler);

      await server.listen(getTmpSocketAdress());
      expect(afterCloseHandler.notCalled).to.be.true;

      await server.close();
      expect(afterCloseHandler.calledOnce).to.be.true;
    });

    it('sets multiple handlers to be called after server closed.', async () => {
      const server = xpress();
      const firstAfterCloseHandler = sinon.spy();
      const secondAfterCloseHandler = sinon.spy();
      server.afterClose(firstAfterCloseHandler);
      server.afterClose(secondAfterCloseHandler);

      await server.listen(getTmpSocketAdress());

      expect(firstAfterCloseHandler.notCalled).to.be.true;
      expect(secondAfterCloseHandler.notCalled).to.be.true;

      await server.close();

      expect(firstAfterCloseHandler.calledOnce).to.be.true;
      expect(secondAfterCloseHandler.calledOnce).to.be.true;
    });

    it('executes handlers consecutively after server closed.', async () => {
      const server = xpress();
      const firstAfterCloseHandler = sinon.spy();
      const secondAfterCloseHandler = sinon.spy();
      server.afterClose(firstAfterCloseHandler);
      server.afterClose(secondAfterCloseHandler);

      await server.listen(getTmpSocketAdress());
      await server.close();

      expect(firstAfterCloseHandler.calledBefore(secondAfterCloseHandler)).to.be.true;
      expect(secondAfterCloseHandler.calledAfter(firstAfterCloseHandler)).to.be.true;
    });

    it('runs afterClose handler if start fails', async () => {
      const server = xpress();
      const afterCloseHandler = sinon.spy();

      server.beforeListen(faultyLifecycleHandler);
      server.afterClose(afterCloseHandler);

      try {
        await server.listen(getTmpSocketAdress());
      } catch (err) {
        expect(err.message).to.equal(lifecycleErrorMessage);
        expect(afterCloseHandler.calledOnce).to.be.true;
      }
    });

    it('will stop further execution if a handler throws.', async () => {
      const server = xpress();
      const firstCloseHandler = sinon.spy();
      const thirdCloseHandler = sinon.spy();

      server.afterClose(firstCloseHandler);
      server.afterClose(faultyLifecycleHandler);
      server.afterClose(thirdCloseHandler);

      try {
        await server.listen(getTmpSocketAdress());
        await server.close();
      } catch (e) {
        expect(firstCloseHandler.calledOnce).to.be.true;
        expect(thirdCloseHandler.notCalled).to.be.true;
      }
    });

    it('throws if an error happens during shutdown', async () => {
      const server = xpress();

      server.afterClose(faultyLifecycleHandler);

      try {
        await server.listen(getTmpSocketAdress());
        await server.close();
      } catch (err) {
        expect(err.message).to.equal(lifecycleErrorMessage);
      }
    });
  });

  describe('manageProcess', () => {
    it('terminates process with exit code 0', () => {
      const { status } = spawnSync('node', [
        path.resolve(__dirname, 'subprocessFixtures', 'regularClose.fixture.js'),
      ]);
      // console.log(stderr.toString());
      // console.log(stdout.toString());
      expect(status).to.equal(0);
    });

    it('terminates process on error with exit code 1', () => {
      const res = spawnSync('node', [
        path.resolve(__dirname, 'subprocessFixtures', 'errorClose.fixture.js'),
      ]);
      expect(res.status).to.equal(1);
    });

    ['SIGTERM', 'SIGUSR2', 'SIGHUP', 'SIGINT'].forEach((signal) => {
      it(`[WIP] exits gracefully on ${signal} with exit code 0.`, (done) => {
        const child = spawn('node', [
          path.resolve(__dirname, 'subprocessFixtures', 'regularStart.fixture.js'),
        ], { stdio: ['pipe', 'pipe', 'pipe', 'ipc'] });

        child.on('message', ({ type }) => {
          if (type === 'listening') {
            process.kill(child.pid, signal);
          }
        });

        // child.stderr.on('data', data => process.stdout.write(data.toString()));
        // child.stdout.on('data', data => process.stdout.write(data.toString()));

        child.on('exit', (exitCode) => {
          expect(exitCode).to.equal(0);
          done();
        });
      });

      it(`runs afterClose handler on signal ${signal}.`, (done) => {
        const messageSpy = sinon.spy();
        const child = spawn('node', [
          path.resolve(__dirname, 'subprocessFixtures', 'regularStart.fixture.js'),
        ], { stdio: ['pipe', 'pipe', 'pipe', 'ipc'] });

        child.on('message', ({ type }) => {
          if (type === 'listening') {
            process.kill(child.pid, signal);
          }
        });

        child.on('message', ({ type }) => {
          if (type === 'afterClose') messageSpy();
        });
        // child.stderr.on('data', data => process.stdout.write(data.toString()));
        // child.stdout.on('data', data => process.stdout.write(data.toString()));

        child.on('exit', () => {
          expect(messageSpy.calledOnce).to.be.true;
          // expect(code).to.equal(0);
          done();
        });
      });
    });
  });
});
