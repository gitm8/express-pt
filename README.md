# express-pt

`express power tools`: express with batteries included.

## Features
* lifecycle hooks (beforeListen, afterClose)
* gracefull shutdown
* Signal handling

## install
```shell
npm i express-pt
```
## usage (express style)

```javascript
const xpress = require('express-pt')

const app = xpress()

// do app configuration

const server = app.listen(<PORT/HANDLE/PATH>, () => {
  console.log('server started')

  server.close(() => {
    console.log('server stopped')
  })
})

```
## usage (ant style)

```javascript
const xpress = require('express-pt');

const app = xpress();


app.listen(<PORT/HANDLE/PATH>)
  .then(() => {
    console.log('ant started')
    return app.close()
  })
  .then(() => {
    console.log('ant stopped')
  })
  .catch((err) => {
    console.log('something went wrong')
  })
```

## additions to [express API](https://expressjs.com/en/api.html)

### app.listen
> Method

If no callback is provided `app.listen(args)` will return a promise instead of the `net.Server` instance (which is available via `app.get('instance')`). The promise resolves to nothing and will throw if an error occured during startup.

If the handle or port that you want the app to listen on is already in use (resulting in an `EADDRINUSE` error), `app.listen` will retry to listen on the handle/port a configurable amount of times.


## Roadmap
* configurable through environment variables
* NODE_ENV=production by default
* logging
* helmet
* monitoring / stats
* request id
* use [memwatch](https://www.npmjs.com/package/memwatch)
* create heapdumps on uncaught exceptions/rejections
